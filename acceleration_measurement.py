import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Flip3: datasheets/setup_Flip3.json
# Watch3: datasheets/setup_Watch3.json

device = 'Flip3'  # Flip3 or Watch3

path_setup_json = "/home/pi/lavalrotor_home_experiment/datasheets/setup_Flip3.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)
    print()

# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

data = {}
sensor_uuid = sensor_settings_dict.get('ID','name')   #['accelerometer']['1ee847be-fddd-6ee4-892a-68c4555b0981']
data[sensor_uuid] = {
        "acceleration_x": [],
        "acceleration_y": [],
        "acceleration_z": [],
        "timestamp": []
}

# Ausgabe des erstellten leeren Dictionaries
print(data)
print()

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Startzeitpunkt der Messung
start_time = time.time()

# Hauptmessschleife
while time.time() - start_time < measure_duration_in_s:
    
        # Lesen der Sensorwerte
        acceleration = accelerometer.acceleration
        
        timestamp = time.time() - start_time

        # Hinzufügen der Werte zur entsprechenden Liste im Dictionary
        data[sensor_uuid]["acceleration_x"].append(acceleration[0])
        data[sensor_uuid]["acceleration_y"].append(acceleration[1])
        data[sensor_uuid]["acceleration_z"].append(acceleration[2])
        data[sensor_uuid]["timestamp"].append(timestamp)

        # 1 ms Pause nach jedem Schleifendurchlauf
        time.sleep(0.001)

# Ausgabe des gefüllten Sensor-Daten-Dictionarys
print(data)
print()

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
with h5py.File(path_h5_file, "w") as h5_file:
        # raw_data_group = f.create_group('RawData')

        for sensor_uuid, sensor_data in data.items():
            sensor_group = h5_file.create_group(sensor_uuid)
            for key, value in sensor_data.items():
                sensor_group.create_dataset(key, data=np.array(value))
                
print("Messdaten erfolgreich in der HDF5-Datei gespeichert.")

"""
            # Speichern der Beschleunigungsdaten
            subgroup.create_dataset('acceleration_x', data=data['acceleration_x'], dtype='f', compression='gzip')
            subgroup.create_dataset('acceleration_y', data=data['acceleration_y'], dtype='f', compression='gzip')
            subgroup.create_dataset('acceleration_z', data=data['acceleration_z'], dtype='f', compression='gzip')

            # Hinzufügen des Einheitenattributs
            subgroup['acceleration_x'].attrs['unit'] = 'm/s^2'
            subgroup['acceleration_y'].attrs['unit'] = 'm/s^2'
            subgroup['acceleration_z'].attrs['unit'] = 'm/s^2'

            # Speichern der Zeitstempeldaten
            subgroup.create_dataset('timestamp', data=data['timestamp'], dtype='f', compression='gzip')

            # Hinzufügen des Einheitenattributs
            subgroup['timestamp'].attrs['unit'] = 's'
"""

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))