Adafruit_Blinka==8.20.1
adafruit_circuitpython_adxl34x==1.12.10
h5py==2.10.0
h5py._debian_h5py_serial==2.10.0
matplotlib==3.7.2
numpy==1.21.0
numpy==1.19.5
uuid6==2023.5.2