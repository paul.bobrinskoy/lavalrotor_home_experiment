"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    # Berechnung des Betrages der Beschleunigung zu jedem Zeitpunkt
    accel_magnitude = np.sqrt(x**2 + y**2 + z**2)

    return accel_magnitude

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    # Erstellung einer Linearen Interpolationsfunktion
    interpolation_function = np.interp

    # Berechnung der äquidistanten Zeitpunkte für die Interpolation
    interpolated_time = np.linspace(min(time), max(time), len(time))

    # Führung der lineare Interpolation durch
    interpolated_data = interpolation_function(interpolated_time, time, data)

    return interpolated_time, interpolated_data


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    # Subtrahierung des Mittelwertes von x
    x_mean_subtracted = x - np.mean(x)

    # Berechnung der FFT
    fft_result = np.fft.fft(x_mean_subtracted)

    # Berechnnung der Frequenzen
    frequencies = np.fft.fftfreq(len(time), time[1] - time[0])

    # Index für positive Frequenzen extrahieren
    positive_freq_index = np.where(frequencies > 0)

    # Extrahierung der Amplituden und Frequenzen für positive Frequenzen
    fft_amplitude = np.abs(fft_result[positive_freq_index])
    fft_frequency = frequencies[positive_freq_index]

    return fft_amplitude, fft_frequency